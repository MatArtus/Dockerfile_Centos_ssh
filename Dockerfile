FROM centos:7

RUN yum update -y && yum install -y openssh-server  && mkdir /var/run/sshd &&\
	 useradd test && echo "test" | passwd --stdin test

#USER test

RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N '' \
    && ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key -N ''\
    && ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''\
    && ssh-keygen -t dsa -f /etc/ssh/ssh_host_ed25519_key -N ''

EXPOSE 22 

CMD ["/usr/sbin/sshd", "-D"]   

